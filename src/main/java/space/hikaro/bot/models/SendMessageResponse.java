package space.hikaro.bot.models;

public class SendMessageResponse {
    public boolean ok;
    public Message result;
    public String description;
    public int error_code;
}
