package space.hikaro.bot.models;

public class Chat {
    public long id;
    public String first_name;
    public String last_name;
    public String username;
    public String type;
}
