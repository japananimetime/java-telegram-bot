package space.hikaro.bot.models;

public class GetUpdatesResponse {
    public boolean ok;
    public Update[] result;
}
