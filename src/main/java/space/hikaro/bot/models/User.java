package space.hikaro.bot.models;

public class User {
  public long id;
  public boolean is_bot;
  public String first_name;
  public String last_name;
  public String username;
  public String language_code;
}
