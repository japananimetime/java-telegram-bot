package space.hikaro.bot.models;

public class Message {
    public int message_id;
    public User from;
    public Chat chat;
    public String text;
    public int date;
    public Message reply_to_message;
}
