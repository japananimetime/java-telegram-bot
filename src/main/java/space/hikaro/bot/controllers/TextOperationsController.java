package space.hikaro.bot.controllers;

import com.google.gson.Gson;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import space.hikaro.bot.models.SendMessageResponse;
import space.hikaro.bot.models.Update;
import space.hikaro.bot.services.TelegramService;
import space.hikaro.bot.services.TextOperationsService;

import java.io.IOException;

@RestController
public class TextOperationsController {

    @PostMapping("/webhook")
    private String webhook(@RequestBody() String body) throws IOException {
        System.out.println(body);
        Update data = new Gson().fromJson(body, Update.class);

        if(data.message == null) {
            System.out.println("No message");
            return "No message";
        }
        if (data.message.text == null) {
            System.out.println("No text");
            return "No text";
        }
        if (data.message.text.isEmpty() || data.message.text.isBlank() || data.message.text.equals(" ")) {
            System.out.println("Empty text");
            return "Empty text";
        }
        if(data.message.text.startsWith("/punto")) {
            SendMessageResponse telegramResponse;

            if (data.message.reply_to_message != null) {
                String responseText = TextOperationsService.convertLayout(data.message.reply_to_message.text);
                System.out.println("Original text: " + data.message.reply_to_message.text);
                System.out.println("Converted text: " + responseText);

                if(responseText.isEmpty()) {
                    responseText = "No text to convert";
                }
                telegramResponse = TelegramService.sendMessage(data.message.chat.id + "", responseText);
            }
            else {
                String responseText = "No message to reply to";

                telegramResponse = TelegramService.sendMessage(data.message.chat.id + "", responseText);
            }

            if(telegramResponse.ok) {
                return new Gson().toJson(data);
            } else {
                return telegramResponse.description;
            }
        }

        return "Message not sent";
    }
}
