package space.hikaro.bot.services;

public class TextOperationsService {
    public static String convertLayout (String $text) {
        StringBuilder $result = new StringBuilder();
        for (int $i = 0; $i < $text.length(); $i++) {
            char $char = $text.charAt($i);
            switch ($char) {
                case ' ':
                    $result.append(' ');
                    break;
                case 'q':
                    $result.append('й');
                    break;
                case 'w':
                    $result.append('ц');
                    break;
                case 'e':
                    $result.append('у');
                    break;
                case 'r':
                    $result.append('к');
                    break;
                case 't':
                    $result.append('е');
                    break;
                case 'y':
                    $result.append('н');
                    break;
                case 'u':
                    $result.append('г');
                    break;
                case 'i':
                    $result.append('ш');
                    break;
                case 'o':
                    $result.append('щ');
                    break;
                case 'p':
                    $result.append('з');
                    break;
                case 'a':
                    $result.append('ф');
                    break;
                case 's':
                    $result.append('ы');
                    break;
                case 'd':
                    $result.append('в');
                    break;
                case 'f':
                    $result.append('а');
                    break;
                case 'g':
                    $result.append('п');
                    break;
                case 'h':
                    $result.append('р');
                    break;
                case 'j':
                    $result.append('о');
                    break;
                case 'k':
                    $result.append('л');
                    break;
                case 'l':
                    $result.append('д');
                    break;
                case 'z':
                    $result.append('я');
                    break;
                case 'x':
                    $result.append('ч');
                    break;
                case 'c':
                    $result.append('с');
                    break;
                case 'v':
                    $result.append('м');
                    break;
                case 'b':
                    $result.append('и');
                    break;
                case 'n':
                    $result.append('т');
                    break;
                case 'm':
                    $result.append('ь');
                    break;
                case ',':
                    $result.append('б');
                    break;
                case '.':
                    $result.append('ю');
                    break;
                case '/':
                    $result.append('.');
                    break;
                case 'Q':
                    $result.append('Й');
                    break;
                case 'W':
                    $result.append('Ц');
                    break;
                case 'E':
                    $result.append('У');
                    break;
                case 'R':
                    $result.append('К');
                    break;
                case 'T':
                    $result.append('Е');
                    break;
                case 'Y':
                    $result.append('Н');
                    break;
                case 'U':
                    $result.append('Г');
                    break;
                case 'I':
                    $result.append('Ш');
                    break;
                case 'O':
                    $result.append('Щ');
                    break;
                case 'P':
                    $result.append('З');
                    break;
                case '[':
                    $result.append('х');
                    break;
                case ']':
                    $result.append('ъ');
                    break;
                case 'A':
                    $result.append('Ф');
                    break;
                case 'S':
                    $result.append('Ы');
                    break;
                case 'D':
                    $result.append('В');
                    break;
                case 'F':
                    $result.append('А');
                    break;
                case 'G':
                    $result.append('П');
                    break;
                case 'H':
                    $result.append('Р');
                    break;
                case 'J':
                    $result.append('О');
                    break;
                case 'K':
                    $result.append('Л');
                    break;
                case 'L':
                    $result.append('Д');
                    break;
                case ';':
                    $result.append('ж');
                    break;
                case '\'':
                    $result.append('э');
                    break;
                case 'Z':
                    $result.append('Я');
                    break;
                case 'X':
                    $result.append('Ч');
                    break;
                case 'C':
                    $result.append('С');
                    break;
                case 'V':
                    $result.append('М');
                    break;
                case 'B':
                    $result.append('И');
                    break;
                case 'N':
                    $result.append('Т');
                    break;
                case 'M':
                    $result.append('Ь');
                    break;


                case 'й':
                    $result.append('q');
                    break;
                case 'ц':
                    $result.append('w');
                    break;
                case 'у':
                    $result.append('e');
                    break;
                case 'к':
                    $result.append('r');
                    break;
                case 'е':
                    $result.append('t');
                    break;
                case 'н':
                    $result.append('y');
                    break;
                case 'г':
                    $result.append('u');
                    break;
                case 'ш':
                    $result.append('i');
                    break;
                case 'щ':
                    $result.append('o');
                    break;
                case 'з':
                    $result.append('p');
                    break;
                case 'ф':
                    $result.append('a');
                    break;
                case 'ы':
                    $result.append('s');
                    break;
                case 'в':
                    $result.append('d');
                    break;
                case 'а':
                    $result.append('f');
                    break;
                case 'п':
                    $result.append('g');
                    break;
                case 'р':
                    $result.append('h');
                    break;
                case 'о':
                    $result.append('j');
                    break;
                case 'л':
                    $result.append('k');
                    break;
                case 'д':
                    $result.append('l');
                    break;
                case 'я':
                    $result.append('z');
                    break;
                case 'ч':
                    $result.append('x');
                    break;
                case 'с':
                    $result.append('c');
                    break;
                case 'м':
                    $result.append('v');
                    break;
                case 'и':
                    $result.append('b');
                    break;
                case 'т':
                    $result.append('n');
                    break;
                case 'ь':
                    $result.append('m');
                    break;
                case 'б':
                    $result.append(',');
                    break;
                case 'ю':
                    $result.append('.');
                    break;
                case 'Й':
                    $result.append('Q');
                    break;
                case 'Ц':
                    $result.append('W');
                    break;
                case 'У':
                    $result.append('E');
                    break;
                case 'К':
                    $result.append('R');
                    break;
                case 'Е':
                    $result.append('T');
                    break;
                case 'Н':
                    $result.append('Y');
                    break;
                case 'Г':
                    $result.append('U');
                    break;
                case 'Ш':
                    $result.append('I');
                    break;
                case 'Щ':
                    $result.append('O');
                    break;
                case 'З':
                    $result.append('P');
                    break;
                case 'Х':
                    $result.append('[');
                    break;
                case 'Ъ':
                    $result.append(']');
                    break;
                case 'Ф':
                    $result.append('A');
                    break;
                case 'Ы':
                    $result.append('S');
                    break;
                case 'В':
                    $result.append('D');
                    break;
                case 'А':
                    $result.append('F');
                    break;
                case 'П':
                    $result.append('G');
                    break;
                case 'Р':
                    $result.append('H');
                    break;
                case 'О':
                    $result.append('J');
                    break;
                case 'Л':
                    $result.append('K');
                    break;
                case 'Д':
                    $result.append('L');
                    break;
                case 'Ж':
                    $result.append(';');
                    break;
                case 'Э':
                    $result.append('\'');
                    break;
                case 'Я':
                    $result.append('Z');
                    break;
                case 'Ч':
                    $result.append('X');
                    break;
                case 'С':
                    $result.append('C');
                    break;
                case 'М':
                    $result.append('V');
                    break;
                case 'И':
                    $result.append('B');
                    break;
                case 'Т':
                    $result.append('N');
                    break;
                case 'Ь':
                    $result.append('M');
                    break;
            }
        }
        return $result.toString();
    }
}
