package space.hikaro.bot.services;

import com.google.gson.Gson;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import space.hikaro.bot.models.SendMessageResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class TelegramService {
    public static SendMessageResponse sendMessage(String chatId, String text) throws IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("https://api.telegram.org/bot658277856:AAHGJMovVedTEVNxXm9y_OlLoaLYyT1oFQs/sendMessage");
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("chat_id",  chatId));
        nvps.add(new BasicNameValuePair("text",  text));
        httpPost.setEntity(new UrlEncodedFormEntity(nvps,"UTF-8"));

        CloseableHttpResponse response = httpclient.execute(httpPost);

        SendMessageResponse data = new Gson().fromJson(EntityUtils.toString(response.getEntity()), SendMessageResponse.class);

        if (!data.ok) {
            Logger.getGlobal().warning("Message not sent: " + data.description);
        }
        return data;
    }
}
